import express from "express";
import morgan from "morgan";
import fs from "fs";
import cors from "cors";

// Valid file extensions
const VALID_EXT = ["log", "txt", "json", "yaml", "xml", "js"];
const FILES_FOLDER = "./api/files";

const app = express();
app.use(express.json());
app.use(morgan("combined"));
app.use(cors());

// Start server
const PORT = 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`, `http://localhost:${PORT}/`);
  // Check if directory to which we save files exists. If not, create it
  try {
    fs.mkdir("./api", { recursive: false }, (err) => {
      // Directory already exists. Ignore error
      return;
    });

    fs.mkdir(FILES_FOLDER, { recursive: false }, (err) => {
      // Directory already exists. Ignore error
      return;
    });
  } catch (error) {
    console.log(error);
  }
});

// Files CRUD
// Create file
app.post("/api/files", (req, res) => {
  const filename = req.body.filename;
  const content = req.body.content;

  if (!filename || content === undefined) {
    return res.status(400).json({
      message: `Please specify '${
        filename ? "content" : "filename"
      }' parameter`,
    });
  }

  try {
    // Check if file exists
    if (fs.existsSync(FILES_FOLDER + "/" + filename)) {
      return res
        .status(400)
        .json({ message: `File '${filename}' already exists!` });
    }

    // Check extension
    const fileExt = (filename.match(/\.([^.]*?)(?=\?|#|$)/) || [])[1];
    if (!fileExt || !VALID_EXT.includes(fileExt)) {
      return res.status(400).json({
        message: `Invalid extension '${fileExt ? `.${fileExt}` : "none"}'`,
      });
    }

    fs.writeFileSync(`${FILES_FOLDER}/${filename}`, content);
    return res.status(200).json({ message: "File created successfully" });
  } catch (error) {
    return res.status(500).json({ message: "Server error" });
  }
});

// Read all files
app.get("/api/files", (_, res) => {
  try {
    const files = fs.readdirSync(FILES_FOLDER);
    return res.status(200).json({ message: "Success", files });
  } catch (error) {
    return res.status(500).json({ message: "Server error" });
  }
});

// Read one file
app.get("/api/files/:filename", (req, res) => {
  const filename = req.params.filename;

  if (!filename) {
    return res.status(400).json({
      message: `Please specify 'filename' parameter`,
    });
  }

  try {
    // Check if file exists
    if (!fs.existsSync(FILES_FOLDER + "/" + filename)) {
      return res.status(400).json({
        message: `No file with '${filename}' filename found`,
      });
    }

    const content = fs.readFileSync(`${FILES_FOLDER}/${filename}`, "utf8");
    const fileStats = fs.statSync(`${FILES_FOLDER}/${filename}`, "utf8");
    const extension = (filename.match(/\.([^.]*?)(?=\?|#|$)/) || [])[1];
    return res.status(200).json({
      message: "Success",
      filename,
      content,
      extension,
      uploadedDate: fileStats.mtime.toISOString().split(".")[0] + "Z", // Remove milliseconds because example doesn't has them
    });
  } catch (error) {
    return res.status(500).json({ message: "Server error" });
  }
});
